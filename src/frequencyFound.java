import java.util.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class frequencyFound 
{
	public float range = 0.8f;
	public ArrayList<String> getChannel(int[] notAvailable) throws SQLException
	{
		String url = "jdbc:MySql://localhost:3306/tvws?useSSL=false";//Please change the url userId and password
		String userId = "root";
		String password = "cyj";
		Connection con = DriverManager.getConnection(url, userId, password);
	    PreparedStatement pstmt;
	    ArrayList<String> list = new ArrayList();
		for(int i = 0; i < notAvailable.length; i++)
		{
			if(notAvailable[i] == 0)
			{
				String selectQuery = "SELECT * FROM tvws.channel WHERE IDchannel = (?) ";
				pstmt = (PreparedStatement) con.prepareStatement(selectQuery);
				pstmt.setInt(1, i);
				ResultSet rs = pstmt.executeQuery();
				if(rs.next())
					list.add(rs.getInt("IDchannel") + "                      " + rs.getInt("lower") + " MHz --- " + rs.getInt("upper")+ " MHz");		
			}
		}
		return list;
	}
	public void insert(String[] using) throws SQLException
	{
		String url = "jdbc:MySql://localhost:3306/tvws?useSSL=false";//Please change the url userId and password
		String userId = "root";
		String password = "cyj";
		Connection con = DriverManager.getConnection(url, userId, password);
		PreparedStatement pstmt;
		String selectQuery = "INSERT INTO channel values(?,?,?) ";
		pstmt = (PreparedStatement) con.prepareStatement(selectQuery);
		int id = Integer.parseInt(using[0]);
        int low = Integer.parseInt(using[1]);
        int up = Integer.parseInt(using[4]); 
        pstmt.setInt(1, id);
        pstmt.setInt(2, low);
        pstmt.setInt(3, up);
		pstmt.executeUpdate();
	}
	
	public void delete(int id) throws SQLException
	{
		String url = "jdbc:MySql://localhost:3306/tvws?useSSL=false";//Please change the url userId and password
		String userId = "root";
		String password = "cyj";
		Connection con = DriverManager.getConnection(url, userId, password);
		PreparedStatement pstmt;
		String selectQuery = "delete from tvws.channel where IDchannel = ? ";
		pstmt = (PreparedStatement) con.prepareStatement(selectQuery);
        pstmt.setInt(1, id);
		pstmt.executeUpdate();
	}
	
	public ResultSet find(float lat, float lon) throws SQLException
	{
		String url = "jdbc:MySql://localhost:3306/tvws?useSSL=false";//Please change the url userId and password
		String userId = "root";
		String password = "cyj";
		Connection con = DriverManager.getConnection(url, userId, password);
		PreparedStatement pstmt;
		String selectQuery = "select * from tvws.location where latitude BETWEEN ? AND ? AND longitude BETWEEN ? AND ? ";
		pstmt = (PreparedStatement) con.prepareStatement(selectQuery);
		pstmt.setFloat(1, (float)(lat-range));
		pstmt.setFloat(2, (float)(lat+range));
		pstmt.setFloat(3, (float)(lon-range));
		pstmt.setFloat(4, (float)(lon+range));
		ResultSet rs = pstmt.executeQuery();
		return rs;
	}
	/*
	 * The class is open code from Internet used to read data from xlsx file.
	 * @author Wdnnccey
     * The website address is 
     * https://www.cnblogs.com/wdnnccey/p/6476889.html
	 */
	public void add() throws SQLException 
	{
		int count = 0;
		String url = "jdbc:MySql://localhost:3306/tvws?useSSL=false";//Please change the url userId and password
		String userId = "root";
		String password = "cyj";
		Connection con = DriverManager.getConnection(url, userId, password);
		PreparedStatement pstmt;
		File excelFile = new File("/Users/chenyujie/Desktop/tvws123.xlsx"); //替换你文档地址
	    XSSFWorkbook wb = null;
	    try 
	    {
	    	wb = new XSSFWorkbook(new FileInputStream(excelFile));
	    } 
	    catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
	    int numberOfSheets = wb.getNumberOfSheets();
	    String str = "";
	    for (int x = 0; x < numberOfSheets; x++) 
	    {
	    	XSSFSheet sheet = wb.getSheetAt(x);
	        int columnNum = 0;
	        if (sheet.getRow(0) != null) 
	        {
	        	columnNum = 4;
	        	//columnNum = sheet.getRow(0).getLastCellNum() - sheet.getRow(0).getFirstCellNum();
	        }
	        if (columnNum > 0) 
	        {
	        	for (Row row : sheet) 
	        	{
	        		String[] singleRow = new String[columnNum];
	                int n = 0;
	                for (int i = 0; i < columnNum; i++) 
	                {
	                        Cell cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
	                        switch (cell.getCellType()) 
	                        {
	                            case Cell.CELL_TYPE_BLANK:
	                            	singleRow[n] = "";
	                                if (cell == null || cell.equals("") || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) 
	                                {
	                                	System.out.print("<Null>|");
	                                } 
	                                else 
	                                {
	                                	System.out.print(singleRow[n] + "|");
	                                }
	                                break;
	                            
	                            case Cell.CELL_TYPE_BOOLEAN:
	                                singleRow[n] = Boolean.toString(cell.getBooleanCellValue());
	                                System.out.print(singleRow[n] + "|");
	                                break;
	                            
	                                // 数值
	                            case Cell.CELL_TYPE_NUMERIC:
	                                if (DateUtil.isCellDateFormatted(cell)) 
	                                {
	                                    SimpleDateFormat sdf = null;
	                                    if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) 
	                                    {
	                                        sdf = new SimpleDateFormat("HH:mm");
	                                    } 
	                                    else 
	                                    {// 日期
	                                        sdf = new SimpleDateFormat("yyyy-MM-dd");
	                                    }
	                                    Date date = cell.getDateCellValue();
	                                    System.out.print(sdf.format(date) + "|");
	                                } 
	                                else 
	                                {
	                                    cell.setCellType(Cell.CELL_TYPE_STRING);
	                                    String temp = cell.getStringCellValue();
	                                    // 判断是否包含小数点，如果不含小数点，则以字符串读取，如果含小数点，则转换为Double类型的字符串
	                                    if (temp.indexOf(".") > -1) 
	                                    {
	                                        singleRow[n] = String.valueOf(new Double(temp))
	                                                .trim();
	                                        System.out.print(singleRow[n] + "|");
	                                    } 
	                                    else 
	                                    {
	                                        singleRow[n] = temp.trim();
	                                        System.out.print(singleRow[n] + "|");
	                                    }
	                                }
	                                break;
	                            case Cell.CELL_TYPE_STRING:
	                                singleRow[n] = cell.getStringCellValue().trim();
	                                System.out.print(singleRow[n] + "|");
	                                break;
	                            case Cell.CELL_TYPE_ERROR:
	                                singleRow[n] = "";
	                                System.out.print(singleRow[n] + "|");
	                                break;
	                            case Cell.CELL_TYPE_FORMULA:
	                                cell.setCellType(Cell.CELL_TYPE_STRING);
	                                String temp = cell.getStringCellValue();
	                                // 判断是否包含小数点，如果不含小数点，则以字符串读取，如果含小数点，则转换为Double类型的字符串
	                                if (temp.indexOf(".") > -1) 
	                                {
	                                    temp = String.valueOf(new Double(temp)).trim();
	                                    Double cny = Double.parseDouble(temp);//6.2041
	                                    DecimalFormat df = new DecimalFormat("0.00");
	                                    String CNY = df.format(cny);
	                                    System.out.print(CNY + "|");
	                                } 
	                                else 
	                                {
	                                    singleRow[n] = temp.trim();
	                                    System.out.print(singleRow[n] + "|");
	                                }
	                            default:
	                                singleRow[n] = "";
	                                break;
	                        }
	                        n++;
	                    }
	                    System.out.println();
	                    String selectQuery = "INSERT INTO location values(?,?,?,?,?)";
	                    String ch = singleRow[0];
	                    int num1 = Integer.parseInt(singleRow[1]);
	                    float num2 = Float.parseFloat(singleRow[2]); 
	                    float num3 = Float.parseFloat(singleRow[3]); 
	                    pstmt = (PreparedStatement) con.prepareStatement(selectQuery);
	                    count++;
	                    pstmt.setInt(1, count);
	                    pstmt.setString(2, ch);
	                    pstmt.setInt(3, num1);
	                    pstmt.setFloat(4, num2);
	                    pstmt.setFloat(5, num3);
	            		pstmt.executeUpdate();
	                }
	                System.out.println("===========================================================Sheet分割线===========================================================");
	            }
	        }
	    }
	}

