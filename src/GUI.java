import java.util.*;
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
public class GUI 
{
	frequencyFound f;
	private JFrame myFrame;
	private JTextField textfield1;
	private JTextArea jta1, jta2, jta3;
	private JLabel jl1;
	private JList myJlist;
	private JScrollPane jScrollPane1;
	private JButton b1s;
	private JPanel p1, p2, p3;
	ArrayList l = new ArrayList();
	String[] using;
	CardLayout cardlayout=new CardLayout();
	public void go()
	{
		myFrame = new JFrame();
		myFrame.setSize(800,400);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.getContentPane().setLayout(cardlayout);
		myFrame.getContentPane().add(p1 = new JPanel(), "1");
		
		p1.setLayout(new BorderLayout());
		p1.setBackground(new Color(0,0,0));
		 
		jta1 = new JTextArea();
		jta1.setForeground(Color.white);
		jta1.setBackground(Color.black);
        jta1.setEditable(false);
        jta1.setText("Please input your address first !!!");
        
		textfield1=new JTextField(10);
		textfield1.setBackground(Color.black);
		textfield1.setForeground(Color.white);
		textfield1.addActionListener(new Field1Listener());
		
		jScrollPane1 = new JScrollPane();
		jScrollPane1.setBackground(Color.black);
		jScrollPane1.setPreferredSize(new java.awt.Dimension(150,150));
		
		myJlist = new JList();
		myJlist.setBackground(Color.black);
		myJlist.setForeground(Color.white);
		myJlist.addMouseListener(new MouseMonitor());
		jScrollPane1.setViewportView(myJlist); 
		
		p1.add(BorderLayout.NORTH, jta1);
		p1.add(BorderLayout.SOUTH, textfield1);
		p1.add(BorderLayout.CENTER, jScrollPane1);
		
		myFrame.setVisible(true);
		cardlayout.show(myFrame.getContentPane(),"1");
		use();
	}

	
	public void use()
	{
		myFrame.getContentPane().add(p2 = new JPanel(),"2");
		p2.setLayout(new BorderLayout());
		p2.setBackground(Color.black);
		
		jta2 = new JTextArea();
		jta2.setForeground(Color.white);
		jta2.setBackground(Color.black);
        jta2.setEditable(false);
        
        jl1 = new JLabel();
		jl1.setForeground(Color.white);
		jl1.setBackground(Color.black);
        jl1.setText("                                                                                  enjoy using the frequency !!!");
        
		JButton b1s = new JButton("stop");
		b1s.setBackground(Color.white);
		b1s.setForeground(Color.black);
		
		p3 = new JPanel();
		p3.setLayout(new BorderLayout());
		p3.add(BorderLayout.CENTER, b1s);
		p3.setForeground(Color.white);
		p3.setBackground(Color.black);
		
		p2.add(BorderLayout.NORTH, jta2);
		p2.add(BorderLayout.CENTER, jl1);
		p2.add(BorderLayout.SOUTH, p3);
		
		b1s.addActionListener(
		new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					f.insert(using);
		            jta1.setText("Please input your address first !!!");
		            cardlayout.show(myFrame.getContentPane(),"1");
		        }
		        catch (Exception ex) 
				{
		            ex.printStackTrace();
			    }
			}
		}
		); 
	}
	
	public class MouseMonitor implements MouseListener
	{
		 public void mouseEntered(MouseEvent me){}
		 public void mousePressed(MouseEvent me){}
		 public void mouseReleased(MouseEvent me) {}
		 public void mouseExited(MouseEvent me) {}
		 public void mouseClicked(MouseEvent me)
		 {
			 JList myList = (JList) me.getSource();
	         int index = myList.getSelectedIndex();    //已选项的下标
	         Object obj = myList.getModel().getElementAt(index);  //取出数据
	         jta2.setText("You have choose the frequency :" + obj.toString());
	         using = obj.toString().split(" +");
	         cardlayout.show(myFrame.getContentPane(),"2");
	         myJlist.setModel(new DefaultComboBoxModel(new String[0]));
	         textfield1.setText("");
	         try
	         {
	        	 f.delete(Integer.parseInt(using[0]));
	         }
	         catch (Exception ex) 
	         {
	        	 ex.printStackTrace();
			 }
		 }	
	}
	
	
	
	public class Field1Listener implements ActionListener 
    {
    	public void actionPerformed(ActionEvent e) 
			{
				String text=textfield1.getText();
				Map<String, Double> coords;
		        coords = OpenStreetMapUtils.getInstance().getCoordinates(text);
		        String latitude = "" + coords.get("lat");
		        String longitude = "" + coords.get("lon");
		        jta1.setText(" latitude    :" + latitude + "\n longitude :" + longitude);
		        f = new frequencyFound();
		        try
		        {
		        	float lat = Float.parseFloat(latitude);
			        float lon = Float.parseFloat(longitude);
		        	ResultSet rs = f.find(lat, lon);
		        	int[] channelAvailable = new int[52];
		        	//channelAvailable[0] = 1;
		        	//channelAvailable[1] = 1;
		 
		        	while(rs.next())
		    		{
		    
		    			int cha = rs.getInt("channels");
		        		System.out.println(rs.getInt("id")+"  "+rs.getString("uid")+"  "+rs.getInt("channels")+"  "+rs.getFloat("latitude")+"  "+rs.getFloat("longitude"));
		        		channelAvailable[cha] = 1;
		    		}
		        	for(int i = 36; i < 39; i++)
		        	{
		        		channelAvailable[i] = 1;
		        		System.out.println("Radio astronomy  " + i + "  " + lat + "  " + lon);
		        	}
		        	for(int i = 38; i < channelAvailable.length; i++)
		        	{
		        		channelAvailable[i] = 1;
		        		System.out.println("600MHz band  " + i + "  " + lat + "  " + lon);
		        	}
		        	l = f.getChannel(channelAvailable);
		        	String[] str = new String[l.size()];
		        	for(int i = 0; i < l.size(); i++)
		        		str[i] = (String)l.get(i);
		        	myJlist.setModel(new DefaultComboBoxModel(str));
		        	jta1.setText(" latitude    :" + latitude + "\n longitude :" + longitude + "\n" + l.size() + " frequencies available");
		        	l.clear(); 
		        }
		        catch(Exception ex)
		        {
		        	jta1.setText("The address name is wrong, please input again!!!");
		        	textfield1.setText("");
		        }
			}
    }			
}